import pygame
import sys
import os


sourceFileDir = os.path.dirname(os.path.abspath(__file__)) 
FirstLocation = os.path.join(sourceFileDir,'EmptyRoom.png')
char = os.path.join(sourceFileDir, 'char.right.png')
char1rs = os.path.join(sourceFileDir, 'char.right.step1.png')
char2rs = os.path.join(sourceFileDir, 'char.right.step2.png')
char1ls = os.path.join(sourceFileDir, 'char.left.step1.png')
char2ls = os.path.join(sourceFileDir, 'char.left.step2.png')

pygame.init()
screen_info = pygame.display.Info()
screen_width = 2560
screen_height = 1600
player_size = 80
platform_height = 20
player_color = (255, 0, 0)
platform_color = (0, 0, 255)
background_color = (0, 0, 0)


location_texture = pygame.image.load(FirstLocation)
location_width = location_texture.get_width()
location_height = location_texture.get_height()
screen = pygame.display.set_mode((screen_width, screen_height),pygame.FULLSCREEN)
pygame.display.set_caption("Game")

character_image = pygame.image.load(char)
# character_image_right = pygame.image.load('char_right.png')
character_walk_right_textures = [
    pygame.image.load(char1rs),
    pygame.image.load(char2rs)
]
character_walk_left_textures = [
    pygame.image.load(char1ls),
    pygame.image.load(char2ls)
]

walk_animation_index = 0
character_animation_timer = 0
character_animation_delay = 10


new_character_width = 50
new_character_height = 163
character_texture = pygame.transform.scale(character_image, (new_character_width, new_character_height))  # Масштабирование текстуры

start_x = (screen_width - new_character_width) // 2
start_y = (screen_height - new_character_height) // 2
player_x = start_x
player_y = start_y
player_speed = 12

player_y_speed = 0

platform_x = 0
platform_y = screen_height - platform_height
current_character_textures = character_walk_right_textures
running = True
is_facing_left = False

while running:
    #background
    screen.fill(background_color)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    keys = pygame.key.get_pressed()
    if keys[pygame.K_LEFT]:
        player_x -= player_speed
        current_character_textures = character_walk_left_textures
        is_facing_left = True

    if keys[pygame.K_RIGHT]:
        player_x += player_speed
        current_character_textures = character_walk_right_textures
        is_facing_left = False

    if keys[pygame.K_UP]:
        player_y -= player_speed

    if keys[pygame.K_DOWN]:
        player_y += player_speed

    player_collider = pygame.Rect(player_x,player_y,new_character_width,new_character_height)
    location_collider = pygame.Rect((screen_width - location_width) // 2,(screen_height - location_height)//2,location_width,location_height)
    platform_collider = pygame.Rect(platform_x,platform_y,screen_width,platform_height)

    if not player_collider.colliderect(location_collider):
        player_x = max((screen_width - location_width) // 2,min(player_x,(screen_width - location_width) // 2 + location_width - new_character_width))
        player_y = max((screen_height - location_height) // 2, min(player_y,(screen_height - location_height) // 2 + location_height - new_character_height))

    if player_collider.colliderect(platform_collider):
        player_y = platform_y - new_character_height

    character_animation_timer += 1
    if character_animation_timer >= character_animation_delay:
        character_animation_timer = 0
        walk_animation_index = (walk_animation_index + 1) % len(current_character_textures)

    location_x = (screen_width - location_texture.get_width()) // 2
    location_y = (screen_height - location_texture.get_height()) // 2
    screen.blit(location_texture, (location_x, location_y))

    current_character_texture = current_character_textures[walk_animation_index]

    screen.blit(current_character_texture, (int(player_x) , int(player_y)))
    pygame.display.flip()

    pygame.time.Clock().tick(60)

pygame.quit()
sys.exit()
